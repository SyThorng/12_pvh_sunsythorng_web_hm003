"use strict";

var timeShow = document.querySelector(".TimeRun");
const start = setInterval(function () {
  get();
}, 1000);
function get() {
  const con = new Date().toString();
  timeShow.innerHTML = con.slice(0, 25);
}
get();

// -------------------------- Show --------------------------------------

var _Date;
function getStart() {
  var date = new Date();
  var _hour = date.getHours();
  var _minute = date.getMinutes();
  var _second = date.getSeconds();

  if (_hour < 10) {
    _hour = "0" + _hour;
  }
  if (_minute < 10) {
    _minute = "0" + _minute;
  }
  if (_second < 10) {
    _second = "0" + _second;
  }

  document.querySelector(
    ".Time_start"
  ).innerHTML = `${_hour} : ${_minute} : ${_second}`;

  document.getElementById("btnStop").style.display = "block";
  document.getElementById("btnStart").style.display = "none";
  document.getElementById("btnClear").style.display = "none";

  _Date = {
    hour: _hour,
    minute: _minute,
    second: _second,
  };
}

var _Date2;
function getStop() {
  var dd = new Date();
  var _hour = dd.getHours();
  var _minute = dd.getMinutes();
  var _second = dd.getSeconds();

  if (_hour < 10) {
    _hour = "0" + _hour;
  }
  if (_minute < 10) {
    _minute = "0" + _minute;
  }
  if (_second < 10) {
    _second = "0" + _second;
  }

  document.querySelector(".time_stop").innerHTML =
    _hour + " : " + _minute + " : " + _second;

  document.getElementById("btnClear").style.display = "block";
  document.getElementById("btnStop").style.display = "none";
  document.getElementById("btnStart").style.display = "none";

  _Date2 = {
    hour: dd.getHours(),
    minute: dd.getMinutes(),
    second: dd.getSeconds(),
  };

  var min = document.querySelector(".min");
  var real = document.querySelector(".real");

  var minn = _Date2.minute - _Date.minute;

  // minn = 422;

  if (minn >= 0 && minn <= 15) {
    min.innerHTML = minn;
    real.innerHTML = 500;
  } else if (minn >= 16 && minn <= 30) {
    min.innerHTML = minn;
    real.innerHTML = 1000;
  } else if (minn >= 31 && minn <= 60) {
    min.innerHTML = minn;
    real.innerHTML = 1500;
  } else {
    const x = minn % 60;
    const y = Math.floor(minn / 60);

    if (x >= 1 && x <= 15) {
      let x_1 = y * 1500 + 500;

      min.innerHTML = minn;
      real.innerHTML = x_1;
    } else if (x >= 16 && x <= 30) {
      let x_1 = y * 1500 + 1000;

      min.innerHTML = minn;
      real.innerHTML = x_1;
    } else if (x >= 31 && x <= 60) {
      let x_1 = y * 1500 + 1500;

      min.innerHTML = minn;
      real.innerHTML = x_1;
    } else {
      let x_1 = y * 1500;
      real.innerHTML = x_1;
    }
  }
}

function Clear() {
  document.querySelector(".time_stop").innerHTML = "00:00:00";
  document.querySelector(".Time_start").innerHTML = "00:00:00";

  document.getElementById("btnClear").style.display = "none";
  document.getElementById("btnStart").style.display = "block";
  document.getElementById("btnStop").style.display = "none";
  document.querySelector(".real").innerHTML = "0";
  document.querySelector(".min").innerHTML = "0";
}
